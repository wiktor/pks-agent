# PKS Agent

Private Key Store proxy that caches unlocked keys for scenarios where key cannot be unlocked interactively.

Note that this application does *not* by itself work with any cryptographic modules it just proxies access to them.

You will need additional, target PKS server for example [pks-openpgp-card][].

[pks-openpgp-card]: https://gitlab.com/sequoia-pgp/pks-openpgp-card
