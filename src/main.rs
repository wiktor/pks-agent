use actix_web::{error, web, App, Error, HttpRequest, HttpResponse, HttpServer};
use futures::StreamExt;
use std::sync::Arc;
use std::{collections::HashMap, sync::Mutex};

const MAX_SIZE: usize = 262_144; // max payload size is 256k

#[derive(Debug)]
struct MyData {
    locations: HashMap<String, String>,
}

async fn greet(
    req: HttpRequest,
    mut payload: web::Payload,
    data: web::Data<Arc<Mutex<MyData>>>,
) -> Result<HttpResponse, Error> {
    eprintln!("{} {}", req.method(), req.path());
    {
        eprintln!("{:#?}", data.lock());
    }
    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    if body.is_empty() {
        eprintln!("body is empty: lookup of {}", req.path());
        let data = data.lock().unwrap();
        return if let Some(loc) = data.locations.get(req.path()) {
            eprintln!("body is empty: lookup! found: {}", loc);
            Ok(HttpResponse::Ok()
                .append_header(("Location", loc.to_string()))
                .body(vec![]))
        } else {
            eprintln!("body is empty: not found");
            Ok(HttpResponse::BadRequest().into())
        };
    }

    use hyper::{Body, Client, Request};
    use hyperlocal::{UnixClientExt, Uri};

    let client = Client::unix();

    let uri: hyper::Uri = Uri::new("/run/user/1000/pks-openpgp-card.sock", req.path()).into();

    let request = Request::builder()
        .method("POST")
        .uri(uri)
        .body(Body::from(body.to_vec()))?;

    let response = client.request(request).await.unwrap();

    if !response.status().is_success() {
        //return Err(Error::KeyUnlockFailed(response.status().into()).into());
    }

    if let Some(loc) = response.headers().get("Location") {
        let location = loc.to_str().unwrap();

        eprintln!(
            "Got location header: {}, saving to {}",
            location,
            req.path()
        );

        {
            let mut data = data.lock().unwrap();
            data.locations
                .insert(req.path().to_string(), location.to_string());
            eprintln!("{:#?}", data);
        }
        {
            eprintln!("{:#?}", data.lock());
        }

        eprintln!("Set {} to {}", req.path().to_string(), location.to_string());

        Ok(HttpResponse::Ok()
            .content_type("text/plain")
            .append_header(("Location", location))
            .body(vec![]))
    } else {
        Ok(HttpResponse::BadRequest().into())
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    use std::os::unix::io::FromRawFd;

    let fds = std::env::var("LISTEN_FDS").unwrap();

    let fds: i32 = fds.parse().unwrap();
    let listener = unsafe { std::os::unix::net::UnixListener::from_raw_fd(fds + 2) };
    listener.set_nonblocking(true).unwrap();

    let hash = HashMap::new();

    let data = web::Data::new(Arc::new(Mutex::new(MyData { locations: hash })));

    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .route("/", web::get().to(greet))
            .route("/{name}", web::post().to(greet))
    })
    //.bind(("127.0.0.1", 8080))?
    //.bind_uds("/run/user/1000/pks-agent.sock")?
    .listen_uds(listener)?
    .run()
    .await
}
